package config

type Config struct {
	Mqtt MqttConfig
	Http HttpConfig
}
