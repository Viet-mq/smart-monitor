package config

type MqttConfig struct {
	Host            string
	Port            int
	KeepAlive       int
	ConnectTimeout  int
	SessionProvider string
	Authenticator   string
	TopicsProvider  string
	Username        string
	Password        string
}
