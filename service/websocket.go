package service

import (
	"fmt"
	"net"
	"net/http"

	"github.com/gorilla/websocket"
)

var wsUpgrader = websocket.Upgrader{
	Subprotocols: []string{"mqtt"},
	CheckOrigin:  func(r *http.Request) bool { return true },
}

// WebsocketHandler provides a http.Handler that forwards MQTT websocket
// requests to a plain TCP socket.
type WebsocketHandler struct {
	// target address, e.g. ":1883"
	Addr string
}

// ServeHTTP implements http.Handler.
func (wh *WebsocketHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// upgrade connection to websocket
	wscon, err := wsUpgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Printf("Upgrade to websocket failed (remote %s): %v\n", r.RemoteAddr, err)
		return
	}
	fmt.Printf("Websocket connection from %s", wscon.RemoteAddr())
	defer fmt.Printf("Closing websocket connection from %s\n", wscon.RemoteAddr())
	defer wscon.Close()

	// connect to MQTT server
	tcpcon, err := net.Dial("tcp", wh.Addr)
	if err != nil {
		fmt.Printf("Connecting websocket to %s failed: %v\n", wh.Addr, err)
		return
	}
	defer tcpcon.Close()

	// send to websocket
	go func() {
		defer wscon.Close()
		defer tcpcon.Close()
		for {
			buffer := make([]byte, 1024)
			n, err := tcpcon.Read(buffer)
			if err != nil || n == 0 {
				return
			}
			err = wscon.WriteMessage(websocket.BinaryMessage, buffer[:n])
			if err != nil {
				return
			}
		}
	}()

	// receive from websocket
	for {
		msgType, msg, err := wscon.ReadMessage()
		if err != nil {
			return
		}
		if msgType != websocket.BinaryMessage {
			fmt.Printf("Non binary websocket message received from %s\n", wscon.RemoteAddr())
		}
		n, err := tcpcon.Write(msg)
		if err != nil || n == 0 {
			return
		}
	}
}
