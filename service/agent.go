package service

import (
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/google/uuid"
	"log"
	"smart-monitor/config"
	"smart-monitor/model"
	"time"
)

var (
	messagePubHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
		fmt.Printf("Message \"%s\" received on topic  \"%s\"\n", msg.Payload(), msg.Topic())
	}
	connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
		fmt.Println("Connected")
	}
	connectionLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
		fmt.Printf("Connection Lost: %s\n", err.Error())
	}

	cfg, _ = config.ReadConfig("config", "config")
)

type Agent struct {
	ID        string
	clientOpt *mqtt.ClientOptions
	MsgChan   chan model.Data
}

func NewAgent(broker string, port int, msgChan chan model.Data) (agent *Agent) {
	agent = &Agent{
		ID:        uuid.NewString(),
		MsgChan:   msgChan,
		clientOpt: mqtt.NewClientOptions(),
	}
	go agent.listen(broker, port)
	return
}

func (agent *Agent) listen(broker string, port int) {

	agent.clientOpt.AddBroker(fmt.Sprintf("tcp://%s:%d", broker, port))
	agent.clientOpt.SetClientID("agent")
	agent.clientOpt.SetKeepAlive(60 * time.Second)
	agent.clientOpt.SetPingTimeout(1 * time.Second)
	agent.clientOpt.SetDefaultPublishHandler(messagePubHandler)
	agent.clientOpt.SetUsername(cfg.Mqtt.Username)
	agent.clientOpt.SetPassword(cfg.Mqtt.Password)
	agent.clientOpt.OnConnect = connectHandler
	agent.clientOpt.OnConnectionLost = connectionLostHandler

	client := mqtt.NewClient(agent.clientOpt)
	token := client.Connect()
	if token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	for {
		select {
		case s := <-agent.MsgChan:
			log.Println(s)
			publish(client, s)
		}
	}

}

func publish(client mqtt.Client, data model.Data) {
	client.Publish(data.Topic, 0, false, data.Data)
}
