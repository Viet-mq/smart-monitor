package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang/gddo/httputil/header"
	"io"
	"log"
	"net/http"
	"smart-monitor/config"
	"smart-monitor/model"
	"smart-monitor/service"
	"strings"
)

var cfg, _ = config.ReadConfig("config", "config")

func main() {

	msgChan := make(chan model.Data)

	service.NewAgent("localhost", cfg.Mqtt.Port, msgChan)

	svr := &service.Server{
		KeepAlive:        cfg.Mqtt.KeepAlive,
		ConnectTimeout:   cfg.Mqtt.ConnectTimeout,
		SessionsProvider: cfg.Mqtt.SessionProvider,
		Authenticator:    cfg.Mqtt.Authenticator,
		TopicsProvider:   cfg.Mqtt.TopicsProvider,
	}

	go startHttpServer(msgChan)

	err := svr.ListenAndServe(fmt.Sprintf("tcp://%s:%d", cfg.Mqtt.Host, cfg.Mqtt.Port))
	if err != nil {
		log.Fatalln("ListenAndServe MQTTServer: ", err)
	}

}

func startHttpServer(msgChan chan model.Data) {
	fmt.Println("Start http server at", cfg.Http.Host, cfg.Http.Port)
	handleSendMessage := sendMessage(msgChan)
	http.HandleFunc("/send-message", handleSendMessage)
	err := http.ListenAndServe(fmt.Sprintf("%s:%d", cfg.Http.Host, cfg.Http.Port), nil)
	if err != nil {
		return
	}
}

func sendMessage(msgChan chan model.Data) func(w http.ResponseWriter, req *http.Request) {

	return func(w http.ResponseWriter, req *http.Request) {

		if req.Header.Get("Content-Type") == "" {
			msg := "Request Content-Type not is application/json"
			http.Error(w, msg, http.StatusBadRequest)
		}

		if req.Header.Get("Content-Type") != "" {
			value, _ := header.ParseValueAndParams(req.Header, "Content-Type")
			if value != "application/json" {
				msg := "Content-Type header is not application/json"
				http.Error(w, msg, http.StatusUnsupportedMediaType)
				return
			}
			req.Body = http.MaxBytesReader(w, req.Body, 1048576)
			dec := json.NewDecoder(req.Body)
			dec.DisallowUnknownFields()
			var p model.Data
			err := dec.Decode(&p)
			if err != nil {
				var syntaxError *json.SyntaxError
				var unmarshalTypeError *json.UnmarshalTypeError
				switch {
				case errors.As(err, &syntaxError):
					msg := fmt.Sprintf("Request body contains badly-formed JSON (at position %d)", syntaxError.Offset)
					http.Error(w, msg, http.StatusBadRequest)
				case errors.Is(err, io.ErrUnexpectedEOF):
					msg := fmt.Sprintf("Request body contains badly-formed JSON")
					http.Error(w, msg, http.StatusBadRequest)
				case errors.As(err, &unmarshalTypeError):
					msg := fmt.Sprintf("Request body contains an invalid value for the %q field (at position %d)", unmarshalTypeError.Field, unmarshalTypeError.Offset)
					http.Error(w, msg, http.StatusBadRequest)
				case strings.HasPrefix(err.Error(), "json: unknown field "):
					fieldName := strings.TrimPrefix(err.Error(), "json: unknown field ")
					msg := fmt.Sprintf("Request body contains unknown field %s", fieldName)
					http.Error(w, msg, http.StatusBadRequest)
				case errors.Is(err, io.EOF):
					msg := "Request body must not be empty"
					http.Error(w, msg, http.StatusBadRequest)
				case err.Error() == "http: request body too large":
					msg := "Request body must not be larger than 1MB"
					http.Error(w, msg, http.StatusRequestEntityTooLarge)
				default:
					log.Println(err.Error())
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
				}
				return
			}
			err = dec.Decode(&struct{}{})
			if err != io.EOF {
				msg := "Request body must only contain a single JSON object"
				http.Error(w, msg, http.StatusBadRequest)
				return
			}

			fmt.Printf("Receive data from topic: '%s', data: '%s' \n", p.Topic, p.Data)
			fmt.Fprint(w, "OK")
			msgChan <- p

		}

	}

}
