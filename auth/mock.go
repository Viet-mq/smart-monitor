package auth

import "smart-monitor/config"

type mockAuthenticator bool

var _ Authenticator = (*mockAuthenticator)(nil)

var (
	mockSuccessAuthenticator mockAuthenticator = true
	mockFailureAuthenticator mockAuthenticator = false
	cfg, _                                     = config.ReadConfig("config", "config")
)

func init() {
	Register("mockSuccess", mockSuccessAuthenticator)
	Register("mockFailure", mockFailureAuthenticator)
}

func (m mockAuthenticator) Authenticate(id string, cred interface{}) error {
	u := cfg.Mqtt.Username
	p := cfg.Mqtt.Password
	if id != u || cred != p {
		return ErrAuthFailure
	}
	if m == true {
		return nil
	}

	return ErrAuthFailure
}
