package auth

import (
	"errors"
	"fmt"
)

var (
	// ErrAuthFailure indicates an authentication failure.
	ErrAuthFailure = errors.New("auth: Authentication failure")

	providers = make(map[string]Authenticator)
)

// Authenticator is the interface for an authentication provider.
type Authenticator interface {
	Authenticate(id string, cred interface{}) error
}

// Register registers an authenticator.
func Register(name string, provider Authenticator) {
	if provider == nil {
		panic("auth: Register provide is nil")
	}

	if _, dup := providers[name]; dup {
		panic("auth: Register called twice for provider " + name)
	}

	providers[name] = provider
}

// Unregister unregisters an authenticator.
func Unregister(name string) {
	delete(providers, name)
}

// Manager manages an authenticator.
type Manager struct {
	p Authenticator
}

// NewManager creates a new manager.
func NewManager(providerName string) (*Manager, error) {
	p, ok := providers[providerName]
	if !ok {
		return nil, fmt.Errorf("session: unknown provider %q", providerName)
	}

	return &Manager{p: p}, nil
}

// Authenticate authenticates a user with credentials.
func (m *Manager) Authenticate(id string, cred interface{}) error {
	return m.p.Authenticate(id, cred)
}
